'use strict'

const StellarSDK = require('stellar-sdk');


StellarSDK.Network.useTestNetwork();
const server = new StellarSDK.Server('https://horizon-testnet.stellar.org');

const SECRET_KEY = 'SBUATL4RSXHLMLCVRCCWKBZSJA3PFC67VYTJXLVTIRALLO7C7SPOB7YC';
const SOURCE_KEYPAIR = StellarSDK.Keypair.fromSecret(SECRET_KEY);
const SOURCE_PUBLIC_KEY = SOURCE_KEYPAIR.publicKey();

module.exports = {
    withdraw: (fromAddress, destination, amount, fee, memo) => {

        return server.loadAccount(SOURCE_PUBLIC_KEY)
        .then((account) => {

            let transaction = new StellarSDK.TransactionBuilder(account)
            .addOperation(StellarSDK.Operation.payment({
                destination: destination,
                asset: StellarSDK.Asset.native(),
                amount: (amount - fee).toString()
            }))
            .addMemo(StellarSDK.Memo.text(memo))
            .build()

            transaction.sign(SOURCE_KEYPAIR);

            return server.submitTransaction(transaction)
            .then((transactionResult) => {
                console.log('Withdraw Successful');
                return transactionResult;
            })
            .catch((err) => {
                console.log('An Error has Occured during Withdrawal : ');
                console.error(err);
                throw new Error('Withdraw Unsuccessful');
            })
        })
        .catch((err) => {
            console.error('An Error has Occured during loadAccount of SOURCE_PUBLIC_KEY');
            console.error(err);
            throw new Error('Withdraw Unsuccessful');
        })
    }
}