const externalStellar = require('./externalStellarService/externalStellar');
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');

const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

const FEE = 0.00001;
const PORT = 8084;
const HOST = '0.0.0.0';

/**
 * Route to withdraw from an external system and deposit into Cryptokart's Wallet.
 * Requires amount, destination = Cryptokart's Wallet Address and memo = Address of the user
 */
app.post('/withdraw', (req, res) => {
    const amount = +req.body.amount;
    const destination = req.body.destination;
    const memo = req.body.memo;

    externalStellar.withdraw('FROM ADDRESS', destination, amount, FEE, memo)
    .then((result) => {
        console.log(result);
        res.status(200).send(result);
    })
    .catch((err) => {
        console.error(err);
        res.status(500).send({error: err.message});
    })

})

app.listen(PORT, HOST, () => {
    console.log(`External Service Running on ${HOST}:${PORT}`);
})