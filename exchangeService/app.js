const walletService = require('./services/walletService');
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');

const app = express();
const COIN = 'stellar';

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false}));

const PORT = 8083;
const HOST = '0.0.0.0';


// ROUTE TO GET A NEW DEPOSIT ADDRESS FOR THE USER. REQUIRES EMAIL ID AS A COMPULSARY FIELD
app.post('/getDepositAddress', (req, res) => {
    const email = req.body.email;

    walletService.getDepositAddress(COIN, email)
    .then((result) => {
        console.log(`New Address generated for email : ${email} : result`);
        res.status(200).send({address: result});
    })
    .catch((err) => {
        console.log(`Error generating address for email : ${email}`);
        res.status(500).send({address: null});
    })
})

// ROUTE TO GET BALANCE OF A USER USING THE RANDOMLY GENERATED ADDRESS FROM THE ABOVE MENTIONED ROUTE
app.post('/getBalance', (req, res) => {
    const address = req.body.address;

    walletService.getBalance(COIN, address)
    .then((result) => {
        console.log(`Fetched balance for address : ${address} : ${result}`);
        res.status(200).send({balance: result});
    })
    .catch((err) => {
        console.log(`Error Fetching balance for address : ${address}`);
        res.status(500).send({balance: null});
    })
})

// ROUTE TO WITHDRAW FUNDS. REFER README FOR MORE DETAILS. REFER stellarService.js FOR MORE DETAILS.
app.post('/withdrawFunds', (req, res) => {
    const fromAddress = req.body.fromAddress;
    const destination = req.body.destination;
    const amount = +req.body.amount;

    walletService.withdrawFunds(COIN, fromAddress, destination, amount)
    .then((result) => {
        res.status(200).send(result);
    })
    .catch((err) => {
        console.log(err);
        res.status(500).send({error: err.message});
    })
})

// ROUTE TO GET DETAILS OF THE WALLET IN THE STELLAR TEST NETWORK
app.get('/getWalletDetails', (req, res) => {

    walletService.getWalletDetails(COIN)
    .then((result) => {
        console.log('Fetched Wallet Details Successfully');
        res.status(200).send(result);
    })
    .catch((err) => {
        console.log('Error in fetching Wallet Details:');
        console.log(err);
        res.status(200).send(err);
    })
})


//EXPRESS APP LISTENING TO THE PORT 8083
app.listen(PORT, HOST, () => {
    console.log(`Cryptokart Stellar Service Running on ${HOST}:${PORT}`);
})