'use strict'

/**
 * REQUIRED PACKAGES
 * 1. Stellar SDK
 * 2. request-promise for making any http request, i.e. to access friend bot to fund a new test account. Not done in this project.
 * 3. MongoDB for storing the new users with their randomly generated addresses and email ID
 */
const StellarSDK = require('stellar-sdk');
const request = require('request-promise');
const MongoClient = require('mongodb').MongoClient;

/**
 * INITIALIZATION of test network
 */
StellarSDK.Network.useTestNetwork(); // to make sure that each bit of stellar service is aware that it's the test network. use '.usePublicNetwork()' for public network
const server = new StellarSDK.Server('https://horizon-testnet.stellar.org'); // stellar test network. use 'horizon.stellar.org' for public net

/**
 * CONSTANTS
 * 1. Stellar SDK allows creation of keypair from the already existing secret key, thus benefiting from not worrying about the public key
 * 2. public key is used for any account related information fetching, or starting a new operation, e.g. payment
 * 3. the keypair is required to finally sign the transaction before submitting, else, the transaction won't be valid
 */
const SECRET_KEY = "SDBTC4UJMI5HLPRHRCGOZLO4XDTQ7EG5D7ZDKFRR7U3ZWCZ36TC3YLS3"; // this is the secret of the account which can be considered as the wallet of Stellar Lumens for Cryptokart
const SOURCE_KEYPAIR = StellarSDK.Keypair.fromSecret(SECRET_KEY);
const SOURCE_PUBLIC_KEY = SOURCE_KEYPAIR.publicKey();
const DB_NAME = 'CRYPTOKART';
const DB_URL = 'mongodb://localhost:27017';


/**
 * FUNCTIONS USED IN THE WHOLE PROJECT FOR STELLAR TEST SERVICE
 */
// FUNCTION TO GET ACCOUNT DETAILS FROM THE LEDGER. INPUT - PUBLIC KEY
function getAccountDetails(publicKey) {
    return server.accounts()
    .accountId(publicKey)
    .call()
    .then((accountResult) => {
        return accountResult;
    })
    .catch((err) => {
        throw new Error(err);
    })
}

// FUNCTION TO GENERATE RANDOM ADDRESS FOR USER ACCOUNT CREATION
function generateRandomAddress(length) {
    const parentString = 'abcdef1234567890';
    const parentStringLength = parentString.length-1;
    let randomAddress = '';

    for(let i=0; i<length; i++)
    {
        randomAddress+= parentString[Math.ceil(Math.random()*parentStringLength)];
    }

    return randomAddress;
}

// FUNCTION TO CREATE A NEW USER IN DB AND RETURN IT'S ADDRESS. INPUT - EMAIL ID - TO LINK A USER TO THE RANDOM ADDRESS - EASE OF ACCESS FOR TESTING
function createUserAndReturnAddress(email) {
    return new Promise((resolve, reject) => {
        MongoClient.connect(DB_URL, { useNewUrlParser: true }, async (err, client) => {
            if(err) {
                reject(err);
            } else {
                const DB = client.db(DB_NAME);
                const USERS = DB.collection('users');
    
                // Creates a new user and attaches the email with the randomly generated address and stores in the MondoDB
                let newUser = {
                    email: email,
                    address: await generateRandomAddress(28),
                    availableLumens: 0
                }
    
                USERS.insertOne(newUser, (err, result) => {
                    if(err) {
                        client.close();
                        reject(err);
                    } else {
                        client.close();

                        //return the newly generated random address
                        resolve(newUser.address);
                    }
                });
            }
        });
    });
}

// FUNCTION GET BALANCE OF THE USER FROM THE DB
function getBalanceOfUser(address) {
    return new Promise((resolve, reject) => {
        MongoClient.connect(DB_URL, { useNewUrlParser: true }, (err, client) => {
            if(err) {
                reject(err);
            } else {
                const DB = client.db(DB_NAME);
                const USERS = DB.collection('users');

                USERS.findOne({address: address}, { projection: {_id: 0, email: 0, address: 0}}, (err, result) => {
                    if(err) {
                        client.close();
                        reject(err);
                    } else {
                        client.close();
                        if(result) {

                            // balance of the user of returned from the DB
                            resolve(result.availableLumens);
                        } else {
                            reject(null);
                        }
                    }
                })
            }
        })
    })
}

// FUNCTION TO SUBTRACT THE AMOUNT OF LUMENS FROM THE DB
function withdrawLumensFromDB(address, amount, fee) {
    return new Promise((resolve, reject) => {
        MongoClient.connect(DB_URL, { useNewUrlParser: true }, (err, client) => {
            if(err) {
                reject(err);
            } else {
                const DB = client.db(DB_NAME);
                const USERS = DB.collection('users');

                // withdraw the amount of lumens from the user's availableLumens in DB
                USERS.findOneAndUpdate({address: address}, {$inc: {availableLumens: -amount}}, (err, result) => {
                    if(err) {
                        client.close();
                        reject(err);
                    } else {
                        if(result) {
                            
                            // if the availableLumens go below the minimal fee requirement, we rollback the transaction and prevent withdrawal of lumens from the blockchain
                            if((result.value.availableLumens - amount) >= fee) {
                                client.close();
                                resolve(true);
                            } else {

                                // update the availableLumens back to their original value and respond saying that the available lumens are less than the amount specified for withdrawal
                                USERS.updateOne({address: address}, {$inc: {availableLumens: amount}}, (err, result) => {
                                    if(err) {
                                        client.close();
                                        reject(err);
                                    } else {
                                        client.close();
                                        reject('Not Enough Lumens Available for Withdrawal');
                                    }
                                })
                            }
                        }
                    }
                })
            }
        })
    })
}

// FUNCTION TO ADD LUMENS TO DB AFTER GETTING THE PAYMENT FROM THE USER
function depositLumensToDB(address, amount) {
    return new Promise((resolve, reject) => {
        MongoClient.connect(DB_URL, { useNewUrlParser: true }, (err, client) => {
            if(err) {
                reject(err);
            } else {
                const DB = client.db(DB_NAME);
                const USERS = DB.collection('users');

                USERS.updateOne({address: address}, {$inc: {availableLumens: amount}}, (err, result) => {
                    if(err) {
                        client.close();
                        reject(err);
                    } else {
                        if(result) {
                            client.close();
                            const resultInJSON = JSON.parse(result.toString());
                            if(!resultInJSON.nModified) {
                                reject('No User Found in the DB');
                            } else {
                                resolve('Lumens Deposited');
                            }
                        }
                    }
                })
            }
        })
    })
}

// FUNCTION TO SAVE THE CURSOR OF THE CURRENTLY PROCESSED PAYMENTS. 
// THIS IS TO ENSURE THAT IF CRYPTOKART SERVER CRASHES AND THE SERVICE IS STARTED AGAIN, PAYMENT PROCESSING WILL BEGIN FROM WHERE IT STOPPED
function savePaymentsCursor(cursor) {
    return new Promise((resolve, reject) => {
        MongoClient.connect(DB_URL, { useNewUrlParser: true }, (err, client) => {
            if(err) {
                reject(err);
            } else {
                const DB = client.db(DB_NAME);
                const CURSORS = DB.collection('cursors');

                CURSORS.updateOne({cursorsInfo: 'payments'}, {$set: {cursorSetAt: cursor}}, {upsert: true}, (err, result) => {
                    if(err) {
                        client.close();
                        reject(err);
                    } else {
                        client.close();
                        resolve('Cursor Updated');
                    }
                })
            }
        })
    })
}


// FUNCTION TO LOAD THE PAYMENTS CURSOR FROM THE DATABASE
function loadPaymentsCursor() {
    return new Promise((resolve, reject) => {
        MongoClient.connect(DB_URL, { useNewUrlParser: true }, (err, client) => {
            if(err) {
                reject(err);
            } else {
                const DB = client.db(DB_NAME);
                const CURSORS = DB.collection('cursors');

                CURSORS.findOne({cursorsInfo: 'payments'}, (err, result) => {
                    if(err) {
                        client.close();
                        reject(err);
                    } else {
                        client.close();
                        if(result)
                            resolve(result.cursorSetAt);
                        else
                            resolve('now'); 
                    }
                })
            }
        })
    })
}

/**
 * Handler Function for the Payments coming from external service into the system via users. e.g. Binance to Cryptokart. 
 * This will capture the payments and update the DB accordingly of the User. 
 */
const paymentHandler = (payment) => {

    console.log('\n ####### RESPONSE OF PAYMENT RECEIVED #############\n');

    // check whether the payment is in or out of the system. If the payment is into the system, it's a deposit by a user and we need to update his/her DB status
    if(payment.to !== SOURCE_PUBLIC_KEY) {          
        return;
    } else {
        const amount = +payment.amount;
        
        payment.transaction()
        .then((transactionResult) => {

            // memo will contain the address of the user which we have generated in our system during the getDepositAddress process to generate an address for a new user entering cryptokart
            const memo = transactionResult.memo;
            
            // save the current cursor of the payment being processed.
            savePaymentsCursor(payment.paging_token);
            
            // update the received lumens from the user into the DB
            depositLumensToDB(memo, amount)
            .then((result) => {
                console.log(`Result of Payment Received from: ${payment.from} || memo: ${memo} :: ${result}\n`);
            })
            .catch((err) => {
                console.error(`Error in Lumens Deposit of the Payment Received from: ${payment.from} || memo: ${memo} :: ${err}\n`);
            })
        })
        .catch((err) => {
            console.error(`Error in Fetching the Transaction Result of the Payment `);
        })
    }
}

/**
 * Start the Payments Monitoring of the incoming payments into Cryptokart System.
 * First, we load the cursor from the database from where we left off the processing of the payments.
 * If no cursor is found, it will begin from the present state and start monitoring the incoming payments.
 * If cursor is found, it will process all the payments from the last processed payment and then save the cursor for future references.
 */
const Payments = server.payments().forAccount(SOURCE_PUBLIC_KEY);

loadPaymentsCursor()
.then((result) => {
    Payments.cursor(result)
    .stream({
        onmessage: paymentHandler
    })
})
.catch((err) => {
    console.error(`Couldn't Load the Payments Cursor. Thus, Payments Stream Monitoring has not begun. Please Restart the Server. Error: ${err}`);
})


/**
 * EXPORTING THE SERVICES IN THE SIMILAR STRUCTURE OF HOW THE OTHER COINS ARE CODED
 * 1. One extra service is the getWalletDetails. It fetches the wallet information from the test network ledger.
 */
module.exports = {

    getWalletDetails: () => {

        /*
        1. Similar to getAccountDetails, except the public key is the CRYPTOKART Wallet public key
        2. Fetches the account details
        3. If error, returns empty object
        */

        return getAccountDetails(SOURCE_PUBLIC_KEY)
        .then((accountData) => {
            return accountData;
        })
        .catch((err) => {
            return {};
        })
    },

    getAccountDetails: (publicKey) => {

        /**
         * 1. Requires the Public key of the account on Ledger of Stellar Test Network
         * 2. Returns the account data, ranging from balance, sequence number, etc.
         * 3. If error, returns empty object
         */

        return getAccountDetails(publicKey)
        .then((accountData) => {
            return accountData;
        })
        .catch((err) => {
            console.log(err);
            return {};
        })        
    },

    getDepositAddress: (email) => {

        /*
        1. Creates user account with email ID
        2. Random address is generated using a simple random generator of hex values
        3. A new account is created and the address is returned for the user
        4. On failure, null is returned
        */

        return createUserAndReturnAddress(email)
        .then((userAddress) => {
            return userAddress;
        })
        .catch((err) => {
            console.log(`Error in New Deposit Address : ${err}`);
            return null;
        })
    },

    getBalance: (address) => {

        /*
        1. Input is the randomly generated address of the user during account creation
        2. On successful fetch, the amount is returned of available lumens belonging to the user
        3. On Failure, null is returned
        */

        return getBalanceOfUser(address)
        .then((result) => {
            return result;
        })
        .catch((err) => {
            console.log(`Error in Fetching Balance from DB : ${err}`);
            return null;
        })
    },

    withdraw: (fromAddress, destination, amount, fee) => {

        /*
        1. First checks whether the user has lumens >= amount in the DB
        2. If True, proceeds to withdrawal from the wallet to the destination address
        3. Assumption : Wallet Lumens are always greater than the amount of withdrawal by any user
        4. On Successful Withdrawal, returns the transaction result with the link to transaction on the testnet of Stellar, transaction hash, etc.
        5. On Failure, the result of failure is returned
        */

        return withdrawLumensFromDB(fromAddress, amount, fee)
        .then((result) => {
            if(result) {

                // enters this block if the withdrawal amount is <= amount present in the DB about the user
                console.log('Lumens Withdrawal from DB Completed. Proceeding to Withdrawal from Wallet to Destination Address...\n');
                return server.loadAccount(SOURCE_PUBLIC_KEY)
                .then((account) => {

                    // loads the account using the public key of stellar wallet
                    // creates a new operation called payment. To transfer lumens from wallet to destination wallet

                    let transaction = new StellarSDK.TransactionBuilder(account)
                    .addOperation(StellarSDK.Operation.payment({
                        destination: destination,
                        asset: StellarSDK.Asset.native(),
                        amount: (amount - fee).toString()
                    }))
                    .build();

                    // the transaction is built for signing and submitting
                    // sign the transaction using the keypair, and not just the secret or public key. 
                    transaction.sign(SOURCE_KEYPAIR);
                    
                    // uncomment to see the built transaction
                    //console.log(transaction.toEnvelope().toXDR('base64')); 
        
                    return server.submitTransaction(transaction)
                    .then((transactionResult) => {

                        // uncomment to see the transaction result
                        //console.log(JSON.stringify(transactionResult, null, 2));
                        //console.log(transactionResult);

                        console.log('Withdraw Successful');

                        // uncomment to get the link of the transaction on the test network ledger
                        //console.log(transactionResult._links.transaction.href);

                        // on successful transaction, the transactionResult is returned
                        return transactionResult;
                    })
                    .catch((err) => {

                        // enters this block if there is any error while submitting the transaction to the stellar test network
                        console.log('An Error has Occured during Withdrawal : ');
                        console.log(err);
                        throw new Error("Withdrawal Unsuccessful");
                    })
                })
                .catch((err) => {

                    // enters this block if there is any error when loading the cryptokart wallet for transaction purpose
                    console.error('An Error has Occured during loadAccount of SOURCE_PUBLIC_KEY');
                    console.error(err);
                    throw new Error("Withdrawal Unsuccessful");
                })                
            }
        })
        .catch((err) => {

            // enters this block if the amount in DB is less than the withdrawal amount
            console.error('An Error has Occurred during DB Withdrawal : ');
            console.error(err);
            throw new Error("Withdrawal Unsuccessful");
        })

    }

}
