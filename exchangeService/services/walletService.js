const StellarService = require('./coins/stellarService');

const _getWalletService = (coin) => {
  switch (coin) {
    case 'stellar': return StellarService;
    default: throw new Error('unknown coin');
  }
};


const _getWithdrawlFee = (coin) => {
  switch (coin) {
    case 'bitcoin': case 'bitcoin-cash': return 0.0001;
    case 'ethereum': return 0.01;
    case 'golem': return 1;
    case 'stellar': return 0.00001; // transaction fee of public stellar network
    default: return 0;
  }
};


module.exports = {
  /**
   * Get a deposit address for a given coin
   */
  getDepositAddress: async function (coin, email) {
    return await _getWalletService(coin).getDepositAddress(email);
  },


  /**
   * Get the balance of a given coin with the given address
   */
  getBalance: async function (coin, address) {
    return await _getWalletService(coin).getBalance(address);
  },


  /**
   * Process a withdrawal
   */
  withdrawFunds: async function (coin, fromAddress, destination, amount) {
    const fee = _getWithdrawlFee(coin);

    if (amount - fee <= 0) {
      throw new Error('not enough funds');
    }

    return await _getWalletService(coin)
      .withdraw(fromAddress, destination, amount, fee);
  },
  
  /**
   * current status of wallet
   */
  getWalletDetails: async (coin) => {

    return await _getWalletService(coin).getWalletDetails();
  }

};

